Information System GUI Server
=============================

Playbook ansible che esegue il provisioning di Information System GUI in un cluster Docker Swarm

Author Information
------------------

- **Giancarlo Panichi** ([ORCID](https://orcid.org/0000-0001-8375-6644)) - <giancarlo.panichi@isti.cnr.it>

License
-------

This project is licensed under the EUPL V.1.2 License - see the [LICENSE](LICENSE) file for details.