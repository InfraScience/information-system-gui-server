#!/bin/bash
#
# The "directory/directory.yml" is the old way that we used to simplify jobs execution.
# The "directory/site.yml" is the syntax used by roles (from ansible version 1.2)
#
# Otherwise we can directly execute a single play (file)
#

PAR=50
TIMEOUT=15
PLAY=site.yml
HOSTS_DIR=.
ANSIBLE_HOSTS=

export TMPDIR=/var/tmp/${USER}
if [ ! -d ${TMPDIR} ] ; then
    mkdir -p ${TMPDIR}
fi

if [ -f ../ansible.cfg ] ; then
    export ANSIBLE_CONFIG="../ansible.cfg"
fi
if [ -f ./ansible.cfg ] ; then
    export ANSIBLE_CONFIG="./ansible.cfg"
fi

# No cows!
export ANSIBLE_NOCOWS=1

export ANSIBLE_ERROR_ON_UNDEFINED_VARS=True
export ANSIBLE_HOST_KEY_CHECKING=False
export ANSIBLE_LIBRARY="/usr/share/ansible:./modules:../modules:$ANSIBLE_LIBRARY"

# Update the galaxy requirements
if [ -f requirements.yml ] ; then
    ansible-galaxy install --force-with-deps -r requirements.yml
fi

PLAY_OPTS="-T $TIMEOUT -f $PAR"

if [ -f "$1"  ] ; then
    PLAY=$1
elif [ ! -f $PLAY ] ; then
    echo "No play file available."
    exit 1
fi

if [  -f "${PLAY}" ] ; then
    MAIN="${PLAY}"
    shift
elif [ -f "${PLAY}.yml" ]; then
    MAIN="${PLAY}.yml"
    shift
fi

if [ -f ${HOSTS_DIR}/hosts ] ; then
    ANSIBLE_HOSTS=${HOSTS_DIR}/hosts
fi
if [ -f ${HOSTS_DIR}/inventory/hosts ] ; then
    ANSIBLE_HOSTS=${HOSTS_DIR}/inventory/hosts
fi
if [ ! -z "$ANSIBLE_HOSTS" ] ; then
   PLAY_OPTS="-i $ANSIBLE_HOSTS"
fi

#echo "Find vault encrypted files if any"
if [ -d ./group_vars ] ; then
    VAULT_GROUP_FILES=$( find ./group_vars -name \*vault\* )
fi
if [ -d ./host_vars ] ; then
    VAULT_HOST_FILES=$( find ./host_vars -name \*vault\* )
fi
 
if [ -n "$VAULT_GROUP_FILES" ] || [ -n "$VAULT_HOST_FILES" ] ; then
    # Vault requires a password.
    # To encrypt a password for a user: python -c "from passlib.hash import sha512_crypt; print sha512_crypt.encrypt('<password>')"
    if [ -f ~/.information_system_gui_ansible_vault_pass.txt ] ; then
        PLAY_OPTS="$PLAY_OPTS --vault-password-file=~/.information_system_gui_ansible_vault_pass.txt"
    else
        echo "There are password protected encrypted files, we will ask for password before proceeding"
        PLAY_OPTS="$PLAY_OPTS --ask-vault-pass"
    fi
fi

# Main
ansible-playbook $PLAY_OPTS $MAIN $@

rm -f /tmp/passwordfile
